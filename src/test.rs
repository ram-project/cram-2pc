#![cfg(test)]

use crate::coordinator::Coordinator;
use crate::worker::Worker;
use crossbeam::channel::unbounded;
use crossbeam::TryRecvError::Empty;

#[test]
fn test_all_commit() {
    let mut participants = Vec::new();
    participants.push(22200);
    participants.push(22201);
    participants.push(22202);

    let mut c = Coordinator::new(11100, participants);

    let f = |_| {};
    Worker::new(22200, |_, _| true, f, f);
    Worker::new(22201, |_, _| true, f, f);
    Worker::new(22202, |_, _| true, f, f);

    let id = c.new_transaction(Vec::new());

    assert!(c.get_result_off(id));
}

#[test]
fn test_all_roolback() {
    let mut participants = Vec::new();
    participants.push(22203);
    participants.push(22204);
    participants.push(22206);

    let mut c = Coordinator::new(11101, participants);

    let f = |_| {};
    Worker::new(22203, |_, _| false, f, f);
    Worker::new(22204, |_, _| false, f, f);
    Worker::new(22205, |_, _| false, f, f);

    let id = c.new_transaction(Vec::new());

    assert!(!c.get_result_off(id));
}

#[test]
fn test_one_roolback() {
    let mut participants = Vec::new();
    participants.push(22207);
    participants.push(22208);
    participants.push(22209);

    let mut c = Coordinator::new(11102, participants);

    let f = |_| {};
    Worker::new(22207, |_, _| false, f, f);
    Worker::new(22208, |_, _| true, f, f);
    Worker::new(22209, |_, _| true, f, f);

    let id = c.new_transaction(Vec::new());

    assert!(!c.get_result_off(id));
}

#[test]
fn test_one_commit() {
    let mut participants = Vec::new();
    participants.push(22210);
    participants.push(22211);
    participants.push(22212);

    let mut c = Coordinator::new(11103, participants);

    let f = |_| {};
    Worker::new(22210, |_, _| false, f, f);
    Worker::new(22211, |_, _| false, f, f);
    Worker::new(22212, |_, _| true, f, f);

    let id = c.new_transaction(Vec::new());

    assert!(!c.get_result_off(id));
}

#[test]
fn test_without_result() {
    let mut participants = Vec::new();
    participants.push(22213);
    participants.push(22214);
    participants.push(22215);
    participants.push(22216);

    let mut c = Coordinator::new(11104, participants);

    let f = |_| {};
    Worker::new(22213, |_, _| true, f, f);
    Worker::new(22214, |_, _| true, f, f);
    Worker::new(22215, |_, _| true, f, f);

    let id = c.new_transaction(Vec::new());

    assert_eq!(c.try_get_result_off(id), None);
}

#[test]
fn test_commit() {
    let (s1, r1) = unbounded(); // commit channel
    let (s2, r2) = unbounded(); // roolback channel

    let mut participants = Vec::new();
    participants.push(22217);

    let mut c = Coordinator::new(11105, participants);

    Worker::new(
        22217,
        |_, _| true,
        move |_| {
            s2.send(1).unwrap();
        },
        move |_| {
            s1.send(1).unwrap();
        },
    );

    let id = c.new_transaction(Vec::new());

    assert!(c.get_result_off(id));

    assert_eq!(r1.recv(), Ok(1));

    assert_eq!(r2.try_recv(), Err(Empty));
}

#[test]
fn test_roolback() {
    let (s1, r1) = unbounded(); // commit channel
    let (s2, r2) = unbounded(); // roolback channel

    let mut participants = Vec::new();
    participants.push(22218);

    let mut c = Coordinator::new(11106, participants);

    Worker::new(
        22218,
        |_, _| false,
        move |_| {
            s2.send(1).unwrap();
        },
        move |_| {
            s1.send(1).unwrap();
        },
    );

    let id = c.new_transaction(Vec::new());

    assert!(!c.get_result_off(id));

    assert_eq!(r2.recv(), Ok(1));

    assert_eq!(r1.try_recv(), Err(Empty));
}
