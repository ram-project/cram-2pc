use crate::message::*;
use cram::node::{Builder, Node};
use std::str::from_utf8;

pub struct Worker {}

impl Worker {
    pub fn new<F, G, H>(port: usize, mut is_prepered: F, mut roolback: G, mut commit: H) -> Node
    where
        F: FnMut(Vec<u8>, i32) -> bool + Send + Sync + 'static,
        G: FnMut(i32) + Send + Sync + 'static,
        H: FnMut(i32) + Send + Sync + 'static,
    {
        let builder = Builder::new(port);

        let shallow_node = builder.get_shallow_node();

        let f = move |v: Vec<u8>| {
            let message: Message = serde_json::from_str(from_utf8(&v).unwrap()).unwrap();
            match message.status {
                0 => {
                    if is_prepered(message.vec, message.id) {
                        Worker::send_ok(&shallow_node, message.id, message.port);
                    } else {
                        Worker::send_rollback(&shallow_node, message.id, message.port);
                    }
                }
                1 => {
                    commit(message.id);
                }
                3 => {
                    roolback(message.id);
                }
                _ => (),
            }
        };

        builder.set_simple_controller_mut(f).build(1)
    }

    fn send_ok(node: &Node, id: i32, to: usize) {
        let mut vec = Vec::new();
        vec.push(OK);

        let msg = Message {
            id: id,
            status: 1,
            port: 0,
            vec: vec,
        };

        let bytes = serde_json::to_string(&msg).unwrap().as_bytes().to_vec();

        node.send(bytes, format!("localhost:{}", to));
    }

    fn send_rollback(node: &Node, id: i32, to: usize) {
        let mut vec = Vec::new();
        vec.push(ROOLBACK);

        let msg = Message {
            id: id,
            status: 3,
            port: 0,
            vec: vec,
        };

        let bytes = serde_json::to_string(&msg).unwrap().as_bytes().to_vec();

        node.send(bytes, format!("localhost:{}", to));
    }
}
