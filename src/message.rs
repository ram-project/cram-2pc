use serde::{Deserialize, Serialize};

pub const ROOLBACK: u8 = 0;
pub const OK: u8 = 1;

#[derive(Serialize, Deserialize)]
pub struct Message {
    pub id: i32,
    pub port: usize,
    pub vec: Vec<u8>,
    pub status: i8,
}
