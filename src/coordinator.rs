use crate::message::*;

use cram::node::{Builder, Node};
use crossbeam::{unbounded, Receiver};
use std::collections::HashMap;
use std::str::from_utf8;

pub struct Coordinator {
    // Workers port
    participants: Vec<usize>,
    // next transation id
    next_id: i32,
    // cram node
    node: Node,
    // my port
    port: usize,
    // results  ( trans_id => is_commit )
    results: HashMap<i32, bool>,
    // channel to send results
    results_recv: Receiver<(i32, bool)>,
}

impl Coordinator {
    pub fn new(port: usize, participants: Vec<usize>) -> Coordinator {
        let (s, r) = unbounded();

        let builder = Builder::new(port);

        let shallow_node = builder.get_shallow_node();

        let part_clone = participants.clone();

        let mut replys: HashMap<i32, (bool, i32)> = HashMap::new();

        let f = move |v: Vec<u8>| {
            // Deserialize message
            let message: Message = serde_json::from_str(from_utf8(&v).unwrap()).unwrap();

            if *message.vec.get(0).unwrap() == OK {
                match replys.remove(&message.id) {
                    Some((is_commit, count)) => {
                        if count + 1 == part_clone.len() as i32 {
                            if is_commit {
                                Coordinator::broadcast_commit(v, &shallow_node, &part_clone);
                                s.send((message.id, true)).unwrap();
                            }
                        } else {
                            replys.insert(message.id, (is_commit, count + 1));
                        }
                    }

                    None => {
                        replys.insert(message.id, (true, 1));
                        if part_clone.len() == 1 {
                            Coordinator::broadcast_commit(v, &shallow_node, &part_clone);
                            s.send((message.id, true)).unwrap();
                        }
                    }
                }
            } else {
                // if is "Roolback"
                match replys.remove(&message.id) {
                    Some((is_commit, count)) => {
                        if count + 1 < part_clone.len() as i32 {
                            replys.insert(message.id, (false, count + 1));
                        }

                        if is_commit {
                            // if is the first roolback
                            Coordinator::broadcast_roolback(v, &shallow_node, &part_clone);
                            s.send((message.id, false)).unwrap();
                        }
                    }
                    None => {
                        replys.insert(message.id, (false, 1));
                        Coordinator::broadcast_roolback(v, &shallow_node, &part_clone);
                        s.send((message.id, false)).unwrap();
                    }
                }
            }
        };

        let node = builder.set_simple_controller_mut(f).build(1);

        Coordinator {
            participants: participants,
            next_id: 0,
            node: node,
            port: port,
            results: HashMap::new(),
            results_recv: r,
        }
    }

    fn broadcast_roolback(bytes: Vec<u8>, node: &Node, participants: &Vec<usize>) {
        // send Message to all
        for worker_port in participants {
            node.send(bytes.clone(), format!("localhost:{}", worker_port));
        }
    }

    fn broadcast_commit(bytes: Vec<u8>, node: &Node, participants: &Vec<usize>) {
        // send Message to all
        for worker_port in participants {
            node.send(bytes.clone(), format!("localhost:{}", worker_port));
        }
    }

    fn broadcast_prepared(
        port: usize,
        node: &Node,
        participants: &Vec<usize>,
        trans_id: i32,
        bytes: Vec<u8>,
        status: i8,
    ) {
        let message = Message {
            id: trans_id,
            port: port,
            vec: bytes,
            status: status,
        };

        let serialized = serde_json::to_string(&message).unwrap().as_bytes().to_vec();

        // send Message to all
        for worker_port in participants {
            node.send(serialized.clone(), format!("localhost:{}", worker_port));
        }
    }

    /// API ///

    pub fn new_transaction(&mut self, bytes: Vec<u8>) -> i32 {
        Coordinator::broadcast_prepared(
            self.port,
            &self.node,
            &self.participants,
            self.next_id,
            bytes,
            0,
        );

        self.next_id += 1;

        self.next_id - 1
    }

    pub fn get_result_off(&mut self, trans_id: i32) -> bool {
        match self.results.get(&trans_id) {
            Some(v) => *v,
            None => {
                let result;
                loop {
                    let (id, v) = self.results_recv.recv().unwrap();
                    self.results.insert(id, v);
                    if id == trans_id {
                        result = v;
                        break;
                    }
                }
                result
            }
        }
    }

    pub fn try_get_result_off(&self, trans_id: i32) -> Option<&bool> {
        self.results.get(&trans_id)
    }
}
